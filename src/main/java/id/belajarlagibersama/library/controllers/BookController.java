package id.belajarlagibersama.library.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import id.belajarlagibersama.library.payloads.request.BookRequest;
import id.belajarlagibersama.library.payloads.response.ResponseData;
import id.belajarlagibersama.library.services.book.BookService;

@RestController
@RequestMapping("/books")

public class BookController {
    @Autowired
    private BookService bookService;

    // membuat object yang diambil dr service
    @PostMapping
    // /books
    public ResponseEntity<Object> createBook(@RequestBody BookRequest request) {
        try {
            ResponseData responseData = bookService.createBookService(request);
            return ResponseEntity.status(responseData.getStatus()).body(responseData);

        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.internalServerError().body(e.getMessage());
        }

    }
    
    @GetMapping
    public ResponseEntity<Object> getBooks(@RequestParam(value="status",defaultValue = "")Boolean status) {
        try {
            ResponseData responseData = bookService.getBookService(status);
            return ResponseEntity.status(responseData.getStatus()).body(responseData);

        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.internalServerError().body(e.getMessage());
        } 
    }
    @GetMapping("/{idBook}")
    public ResponseEntity<Object> getBookById (@PathVariable Long idBook) {
        try {
            ResponseData responseData = bookService.getBookByIdService(idBook);
            return ResponseEntity.status(responseData.getStatus()).body(responseData);
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body(e.getMessage());
            // TODO: handle exception
        }

    }
    @PutMapping("/{idBook}")
    public ResponseEntity<Object> updateBookById(@PathVariable Long idBook,@RequestBody BookRequest request){
        try {
            ResponseData responseData = bookService.updateBookByIdService(idBook,request);
            return ResponseEntity.status(responseData.getStatus()).body(responseData);
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body(e.getMessage());
            // TODO: handle exception
        }
    }

    @DeleteMapping("/{idBook}")
    public ResponseEntity<Object> deleteBook(@PathVariable Long idBook) {
        try {
            ResponseData responseData = bookService.deleteBookService(idBook);
            return ResponseEntity.status(responseData.getStatus()).body(responseData);
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body(e.getMessage());
            // TODO: handle exception
        }
    }
}
