package id.belajarlagibersama.library.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class GreetingController {
    //request param
    @GetMapping("/get")
    //hello?name=dyah
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
      return String.format("Hello %s!", name);
    }

    @PostMapping("/post/{sapa}/{number}")
    //path variabel artinya string variabel sapa jadi key di setelah /post
    // /post/selamat/100
    public String helloPost(@PathVariable ("sapa") String sapa, @PathVariable ("number") int number){
        return "Ini method dari hello Post mapping" +sapa + " " +number;
    }

    //request body
    @PutMapping("/put")
    public String helloPut(@RequestBody String kalimat){
        return "Ini adalah hello put mapping, kalimatnya " + kalimat;
    }
}
