package id.belajarlagibersama.library.payloads.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookRequest {
    //penamaan harus konsisten
    private String judul;
    private String penerbit;
    private String pengarang;
    private String kategori;
    private String tahun;
}
