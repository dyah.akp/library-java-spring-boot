package id.belajarlagibersama.library.models;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "books")
@Data
@NoArgsConstructor
public class Book {
    @Id
    // AUTO INC
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // encapsulation
    private long id;
    @Column(length = 100)
    private String title;
    private String publisher;
    private String author;
    private String category;
    private String year;
    @CreationTimestamp
    @JsonIgnore
    private LocalDateTime createdAt;
    @UpdateTimestamp
    @JsonIgnore
    private LocalDateTime updatedAt;
    private Boolean isDeleted = false;

    // custom constructor
    public Book(String title, String publisher, String author, String category, String year) {
        this.title = title;
        this.publisher = publisher;
        this.author = author;
        this.category = category;
        this.year = year;
    }

}
