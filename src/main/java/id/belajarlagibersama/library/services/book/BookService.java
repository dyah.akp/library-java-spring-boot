package id.belajarlagibersama.library.services.book;

import id.belajarlagibersama.library.payloads.request.BookRequest;
import id.belajarlagibersama.library.payloads.response.ResponseData;

public interface BookService {
    // kerangka methods CRUD
    // Create Book
    ResponseData createBookService(BookRequest bookRequest);

    // Read Books
    ResponseData getBookService(Boolean isDeleted);

    // Read Book by Id
    ResponseData getBookByIdService(Long idBook);

    // Update book
    ResponseData updateBookByIdService(Long idBook, BookRequest request);

    // Delete Book by ID
    ResponseData deleteBookService(Long idBook);

}
