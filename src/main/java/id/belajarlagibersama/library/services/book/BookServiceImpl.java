package id.belajarlagibersama.library.services.book;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import id.belajarlagibersama.library.models.Book;
import id.belajarlagibersama.library.payloads.request.BookRequest;
import id.belajarlagibersama.library.payloads.response.ResponseData;
import id.belajarlagibersama.library.repositories.BookRepository;


@Service
public class BookServiceImpl implements BookService {
    // instance objek yg dibutuhkan
    // autowired
    @Autowired
    BookRepository bookRepository;

    // private Book book=new Book();

    @Override
    public ResponseData createBookService(BookRequest request) {
        Book book=new Book();
        // TODO Auto-generated method stub
        //validasi
        if (request.getJudul() != null) {
            book.setTitle(request.getJudul());
        }
        if (request.getPenerbit() != null) {
            book.setPublisher(request.getPenerbit());
        }
        if (request.getPengarang() != null) {
            book.setAuthor(request.getPengarang());
        }
        if (request.getKategori() != null) {
            book.setCategory(request.getKategori());
        }
        if (request.getTahun() != null) {
            book.setYear(request.getTahun());
        }
        bookRepository.save(book);
        ResponseData responseData = new ResponseData(HttpStatus.CREATED.value(), "Success", book);

        return responseData;
    }

    @Override
    public ResponseData getBookService(Boolean status) {
        // TODO Auto-generated method stub
        List<Book> books;
        if (status==null){
            books= bookRepository.findAll();
        }
        else{
            books= bookRepository.findByIsDeleted(status);
        }
        ResponseData responseData = new ResponseData(HttpStatus.OK.value(),"Success",books);
        return responseData;
    }

    @Override
    public ResponseData getBookByIdService(Long idBook) {
        // TODO Auto-generated method stub
        Optional<Book> bookFind = bookRepository.findById(idBook);   
        ResponseData responseData;
        if (bookFind.isPresent()){
            responseData= new ResponseData(HttpStatus.OK.value(),"Success",bookFind.get());
        }    else{
             responseData = new ResponseData(HttpStatus.NOT_FOUND.value(), "Not Found", null);

        }
         return responseData;
    }

    @Override
    public ResponseData updateBookByIdService(Long idBook, BookRequest request) {
        //find book
        //update book
        // TODO Auto-generated method stub
        Optional<Book> bookFind = bookRepository.findById(idBook);
        ResponseData responseData;
        //validate book
        if(bookFind.isPresent()){
            Book book = bookFind.get();
            if (request.getJudul() != null) {
                book.setTitle(request.getJudul());
            }
            if (request.getPenerbit() != null) {
                book.setPublisher(request.getPenerbit());
            }
            if (request.getPengarang() != null) {
                book.setAuthor(request.getPengarang());
            }
            if (request.getKategori() != null) {
                book.setCategory(request.getKategori());
            }
            if (request.getTahun() != null) {
                book.setYear(request.getTahun());
            }
            bookRepository.save(book);
            responseData = new ResponseData (200,"Success",book);
        }else{

responseData = new ResponseData(404,"Not Found",null);
        }
        return responseData;
    }

    @Override
    public ResponseData deleteBookService(Long idBook) {
        // TODO Auto-generated method stub
        //find book dr opsional book
        Optional<Book> bookFind = bookRepository.findById(idBook);
        ResponseData responseData;
        //validasi buknya ada atau tidak
        if (bookFind.isPresent()){
            //bentuk objek dulu untuk menyimpan data buku yang kita get
            Book book = bookFind.get();
            book.setIsDeleted(true);
            //save untuk update ke db
            bookRepository.save(book);
            responseData = new ResponseData(HttpStatus.OK.value(),"Success",null);
        }else{
            responseData = new ResponseData(HttpStatus.NOT_FOUND.value(),"Not Found", null);

        }

        return responseData;
    }

  

}
